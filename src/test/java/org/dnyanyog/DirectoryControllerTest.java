package org.dnyanyog;
import javax.xml.xpath.XPathExpressionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testng.annotations.Test;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
@SpringBootTest

@AutoConfigureMockMvc


public class DirectoryControllerTest extends AbstractTestNGSpringContextTests {
	@Autowired 
	MockMvc mocMvc;

   @Test(priority=1)
	// new user
	public void verifyUsersSignupSuccessOpertion() throws XPathExpressionException, Exception {
		RequestBuilder requestBuilder=MockMvcRequestBuilders.post("/directory/api/v1/signup").content("<User>\\n\"\r\n"
				+ "  \r\n"
				+ "  +\" <fullName>Sanket Yelam</fullName>\\n\" +\r\n"
				+ "  \"  <email>sanket123@gmail.com</email>\\n\" +\r\n"
				+ "  \"  <password>12344</password>\\n\" + \" <mobile>88888888888</mobile>\\n\"\r\n"
				+ "  + \" <currency>dolar</currency>\\n\" + \" <language>English</language> \\n\" +\r\n"
				+ "  \"  <country>US</country>\\n\" + \" </User>\\n\" +\"").contentType(MediaType.APPLICATION_XML_VALUE).accept(MediaType.APPLICATION_XML_VALUE);
		MvcResult result=mocMvc.perform(requestBuilder).andExpect(status().isCreated())
				  .andExpect(xpath("/SignUpResponse/status").string("success"))
				  .andExpect(xpath("/SignUpResponse/message"). string("user account created successfully"))
				  .andExpect(xpath("/SignUpResponse/data/fullName").string("Sanket Yelam"))
			  .andExpect(xpath("/SignUpResponse/data/email").string("sanket123@gmail.com"))
				  .andExpect(xpath("/SignUpResponse/data/mobile").string("88888888888"))
				  .andExpect(xpath("/SignUpResponse/data/currency").string("dolar"))
				  .andExpect(xpath("/SignUpResponse/data/language").string("English"))
				  .andExpect(xpath("/SignUpResponse/data/country").string("US"))
				  .andExpect(xpath("/SignUpResponse/data/userId").string("1"))
				  .andReturn();
		}
		// email exist
	@Test(priority=2)
	
	public void verifyUsersSignupFailOpertionForDuplicateEmail() throws XPathExpressionException, Exception {		RequestBuilder requestBuilder=MockMvcRequestBuilders.post("/directory/api/v1/signup").content("<User>\\n\"\r\n"
				+ "  \r\n"
				+ "  +\" <fullName>Gaurav Pandarkar</fullName>\\n\" +\r\n"
				+ "  \"  <email>sanket123@gmail.com</email>\\n\" +\r\n"
				+ "  \"  <password>password23</password>\\n\" + \" <mobile>88888888889</mobile>\\n\"\r\n"
				+ "  + \" <currency>USD</currency>\\n\" + \" <language>en</language> \\n\" +\r\n"
				+ "  \"  <country>IN</country>\\n\" + \" </User>\\n\" +\"").contentType(MediaType.APPLICATION_XML_VALUE).accept(MediaType.APPLICATION_XML_VALUE);
		MvcResult result=mocMvc.perform(requestBuilder).andExpect(status().isConflict())
				  .andExpect(xpath("/SignUpResponse/status").string("error"))
				  .andExpect(xpath("/SignUpResponse/message"). string("Email or mobile number already registered"))
				 
				  .andReturn();
		}
    
	//Mobile is exist
			
@Test(priority=4)

public void verifyUsersSignupFailOpertionForDuplicateMobileNumber() throws XPathExpressionException, Exception {
	RequestBuilder requestBuilder=MockMvcRequestBuilders.post("/directory/api/v1/signup").content("<User>\\n\"\r\n"
			+ "  \r\n"
			+ "  +\" <fullName>Sanket Yelam</fullName>\\n\" +\r\n"
			+ "  \"  <email>sanket12@gmail.com</email>\\n\" +\r\n"
			+ "  \"  <password>12344</password>\\n\" + \" <mobile>88888888888</mobile>\\n\"\r\n"
			+ "  + \" <currency>dolar</currency>\\n\" + \" <language>English</language> \\n\" +\r\n"
			+ "  \"  <country>US</country>\\n\" + \" </User>\\n\" +\"").contentType(MediaType.APPLICATION_XML_VALUE).accept(MediaType.APPLICATION_XML_VALUE);
	MvcResult result=mocMvc.perform(requestBuilder).andExpect(status().isConflict())
			  .andExpect(xpath("/SignUpResponse/status").string("error"))
			  .andExpect(xpath("/SignUpResponse/message"). string("Email or mobile number already registered"))
			 
			  .andReturn();
//	
	}
//
	
	@Test (priority=2)
	public void verifyUsersSignupFailOpertionForDuplicateData() throws XPathExpressionException, Exception {
		RequestBuilder requestBuilder=MockMvcRequestBuilders.post("/directory/api/v1/signup").content("<User>\\n\"\r\n"
				+ "  \r\n"
				+ "  +\" <fullName>Sanket Yelam</fullName>\\n\" +\r\n"
				+ "  \"  <email>sanket1234@gmail.com</email>\\n\" +\r\n"
				+ "  \"  <password>12344</password>\\n\" + \" <mobile>88888888888</mobile>\\n\"\r\n"
				+ "  + \" <currency>dolar</currency>\\n\" + \" <language>English</language> \\n\" +\r\n"
				+ "  \"  <country>US</country>\\n\" + \" </User>\\n\" +\"").contentType(MediaType.APPLICATION_XML_VALUE).accept(MediaType.APPLICATION_XML_VALUE);
		MvcResult result=mocMvc.perform(requestBuilder).andExpect(status().isConflict())
				  .andExpect(xpath("/SignUpResponse/status").string("error"))
				  .andExpect(xpath("/SignUpResponse/message"). string("Email or mobile number already registered"))
				 
				  .andReturn();
		}

	@Test(priority=1)
	// new user
	public void createGroupOperation() throws XPathExpressionException, Exception {
		RequestBuilder requestBuilder=MockMvcRequestBuilders.post("/groups/api/v1/create").content("<User>\\n\"\r\n"
				+ "  \r\n"
				+ "  \"  <groupName>W19</groupName>\\r\\n\"\r\n"
				+ "			+ \"  <groupType>springboot devoloper</groupType>\\r\\n\" + \" </User>\\n\" +\"").contentType(MediaType.APPLICATION_XML_VALUE).accept(MediaType.APPLICATION_XML_VALUE);		MvcResult result=mocMvc.perform(requestBuilder).andExpect(status().isCreated())
			  .andExpect(xpath("/GroupResponse/status").string("success"))
				  .andExpect(xpath("/GroupResponse/message"). string("Group created successfully"))
				  .andExpect(xpath("/GroupResponse/data/groupId").string("1"))
				  .andExpect(xpath("/GroupResponse/data/groupName").string("W19"))
				  .andExpect(xpath("/GroupResponse/data/groupType").string("springboot devoloper"))
				  .andReturn();
		}

	
//	//SignUp
// no password set
@Test(priority=2)
public void verifyUsersSignupUpdateOpertion() throws XPathExpressionException, Exception {
	RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/directory/api/v1/signup")
			.content("<User>\n"
					+ "<fullName>Anuja</fullName>\n"
					+ "<email>anuja123@gmail.com</email>\n"
					+ "<password>anuja@123</password>\n" 
					+ "<mobile>123456789</mobile>\n"
					+ "<currency>dolar</currency>\n" 
					+ "<language>English</language>\n" 
					+ "<country>US</country>\n"
					+ "</User>\n")
			.contentType(MediaType.APPLICATION_XML_VALUE).accept(MediaType.APPLICATION_XML_VALUE);

	MvcResult result = mocMvc.perform(requestBuilder).andExpect(status().isCreated())
			.andExpect(xpath("/SignUpResponse/status").string("success"))
			.andExpect(xpath("/SignUpResponse/message").string("user account created successfully"))
			.andExpect(xpath("/SignUpResponse/data/fullName").string("Anuja"))
			.andExpect(xpath("/SignUpResponse/data/email").string("anuja123@gmail.com"))
			.andExpect(xpath("/SignUpResponse/data/mobile").string("123456789"))
			.andExpect(xpath("/SignUpResponse/data/currency").string("dolar"))
			.andExpect(xpath("/SignUpResponse/data/language").string("English"))
			.andExpect(xpath("/SignUpResponse/data/country").string("US"))
			.andExpect(xpath("/SignUpResponse/data/userId").string("2"))
			.andReturn();
}
	

//	@Test(priority=4)
//	public void verifyUserSignupSuccessForCreateFriend() throws XPathExpressionException, Exception {
//		
//		//Create Friend
//		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/friends/api/v1/create")
//				.content("<User>\n"
//						+ "<fullName>Gaurav Pandarkar</fullName>\n"
//						+ "<email>gaurav@gmail.com</email>\n" 
//						+ "<mobile>4444444444</mobile>\n"
//						+ "</User>\n")
//				.contentType(MediaType.APPLICATION_XML_VALUE).accept(MediaType.APPLICATION_XML_VALUE);
//	
//		MvcResult result = mocMvc.perform(requestBuilder).andExpect(status().isCreated())
//				.andExpect(xpath("/FriendResponse/status").string("Success"))
//				.andExpect(xpath("/FriendResponse/massage").string("friend Created Successfully"))
//				.andExpect(xpath("/FriendResponse/data/fullName").string("Gaurav Pandarkar"))
//				.andExpect(xpath("/FriendResponse/data/email").string("gaurav@gmail.com"))
//				.andExpect(xpath("/FriendResponse/data/mobile").string("4444444444"))
//				.andExpect(xpath("/FriendResponse/data/currency").string(""))
//				.andExpect(xpath("/FriendResponse/data/language").string(""))
//				.andExpect(xpath("/FriendResponse/data/country").string(""))
//				.andExpect(xpath("/FriendResponse/data/userId").string("1"))
//				.andReturn();
//	}	


}
//
//RequestBuilder requestBuilder=MockMvcRequestBuilders.post("/directory/api/v1/create").content("<User>\\n\"\r\n"
//		+ "  \r\n"
//		+ "  +\" <fullName>Sanket Yelam</fullName>\\n\" +\r\n"
//		+ "  \"  <email>sanket123@gmail.com</email>\\n\" +\r\n"
//		+ "  \"  <password>12344</password>\\n\" + \" <mobile>88888888888</mobile>\\n\"\r\n"
//		+ "  + \" <currency>dolar</currency>\\n\" + \" <language>English</language> \\n\" +\r\n"
//		+ "  \"  <country>US</country>\\n\" + \" </User>\\n\" +\"").contentType(MediaType.APPLICATION_XML_VALUE).accept(MediaType.APPLICATION_XML_VALUE);
//MvcResult result=mocMvc.perform(requestBuilder).andExpect(status().isConflict())
//		  .andExpect(xpath("/SignUpResponse/status").string("error"))
//		  .andExpect(xpath("/SignUpResponse/message"). string("Email or mobile number already registered"))
//		 
//		  .andReturn();
//
//
//RequestBuilder requestBuilder1=MockMvcRequestBuilders.post("/directory/api/v1/signup").content("<User>\\n\"\r\n"
//		+ "  \r\n"
//		+ "  +\" <fullName>Sanket Yelam</fullName>\\n\" +\r\n"
//		+ "  \"  <email>sanket123@gmail.com</email>\\n\" +\r\n"
//		+ "  \"  <password>12344</password>\\n\" + \" <mobile>88888888888</mobile>\\n\"\r\n"
//		+ "  + \" <currency>dolar</currency>\\n\" + \" <language>English</language> \\n\" +\r\n"
//		+ "  \"  <country>US</country>\\n\" + \" </User>\\n\" +\"").contentType(MediaType.APPLICATION_XML_VALUE).accept(MediaType.APPLICATION_XML_VALUE);
//MvcResult result1=mocMvc.perform(requestBuilder).andExpect(status().isConflict())
//		  .andExpect(xpath("/SignUpResponse/status").string("error"))
//		  .andExpect(xpath("/SignUpResponse/message"). string("Email or mobile number already registered"))
//		 
//		  .andReturn();
