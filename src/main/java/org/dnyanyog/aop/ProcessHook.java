package org.dnyanyog.aop;


import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.dnyanyog.entity.Users;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class ProcessHook {

	@Before("execution(* org.dnyanyog.repository.*.save(..))")
	public void beforeExecution(JoinPoint jointPoint) {
		System.out.println("\n****************Saving object -" + jointPoint.getArgs()[0]);

	//	Users users = (Users) jointPoint.getArgs()[0];
//		System.out.println(users.getFullName());
//		System.out.println(users.getMobile());
//		System.out.println(users.getEmail());
//		System.out.println(users.getPassword());
//		System.out.println(users.getCountry());
//		System.out.println(users.getLanguage());
//		System.out.println(users.getCurrency());

	}

	@After("execution(* org.dnyanyog.repository.*.save(..))")
	public void afterExecution(JoinPoint jointPoint) {
		System.out.println("\n****************Saved object -" + jointPoint.getArgs()[0]);
//		Users users = (Users) jointPoint.getArgs()[0];
//
//		System.out.println(users.getFullName());
//		System.out.println(users.getMobile());
//		System.out.println(users.getEmail());
//		System.out.println(users.getPassword());
//		System.out.println(users.getCountry());
//		System.out.println(users.getLanguage());
//		System.out.println(users.getCurrency());
	}

}