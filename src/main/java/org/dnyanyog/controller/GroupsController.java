package org.dnyanyog.controller;

import org.dnyanyog.dto.request.GroupRequest;
import org.dnyanyog.dto.request.MappingRequest;
import org.dnyanyog.dto.request.SignUpRequest;
import org.dnyanyog.dto.responce.GenericResponse;
import org.dnyanyog.dto.responce.GroupResponse;
import org.dnyanyog.dto.responce.MappingResponse;
import org.dnyanyog.service.GroupService;
import org.dnyanyog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GroupsController {
	@Autowired
	UserService userService;
	@PostMapping(path="groups/api/v1/create",produces= {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE},consumes= {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
	public ResponseEntity<GroupResponse> createGroup(@RequestBody GroupRequest request) {
		return userService.createGroup(request);
		
	}
	@PostMapping(path="groups/api/v1/friends")
	public MappingResponse groupUserMap(@RequestBody MappingRequest request) {
		
		return userService.mappedFriend(request);
	}


}