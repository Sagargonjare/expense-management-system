package org.dnyanyog.dto.request;

import org.springframework.stereotype.Component;

@Component
public class FriendRequest {
	private String fullname;
	private String mobile;
	private String email;
	public String getFull_name() {
		return fullname;
	}
	public void setFull_name(String full_name) {
		this.fullname = fullname;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
}
