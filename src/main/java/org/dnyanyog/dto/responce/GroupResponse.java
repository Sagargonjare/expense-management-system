package org.dnyanyog.dto.responce;

import org.dnyanyog.dto.request.GroupRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
@Component
public class GroupResponse {
	private String status;
	private String message;
	@Autowired
	private GroupData data;
	
	public GroupData getData() {
		return data;
	}
	public void setData(GroupData data) {
		this.data = data;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}


	

}
