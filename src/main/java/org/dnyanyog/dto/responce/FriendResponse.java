package org.dnyanyog.dto.responce;

import org.dnyanyog.dto.request.FriendRequest;
import org.dnyanyog.dto.request.LoginRequest;
import org.springframework.stereotype.Component;
@Component
public class FriendResponse {
	private String status;
	private String massage;
	private FriendRequest data;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMassage() {
		return massage;
	}
	public void setMassage(String massage) {
		this.massage = massage;
	}
	public FriendRequest getData() {
		return data;
	}
	public void setData(FriendRequest data) {
		this.data = data;
	}
	
	
}
