package org.dnyanyog.dto.responce;

import org.springframework.stereotype.Component;

@Component
public class MappingResponse {
	private String status;
	private String message;
	private MappedData data;

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public MappedData getData() {
		return data;
	}
	public void setData(MappedData data) {
		this.data = data;
	}
	
}
