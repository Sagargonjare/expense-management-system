package org.dnyanyog.repository;

import org.dnyanyog.entity.GroupUserMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
   public interface GroupUserMapRepository  extends JpaRepository<GroupUserMapping,Long>{
	

}