package org.dnyanyog.repository;

import org.dnyanyog.entity.GroupInformation;
import org.dnyanyog.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupRepository extends JpaRepository<GroupInformation, Long>{

	GroupInformation save(GroupInformation group);

	GroupInformation findByGroupName(String groupName);
	//GroupInformation findByGroup_Id(String group_Id);


}
